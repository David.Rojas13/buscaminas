class Tablero
{
 IniciarTablero()
 {
    let fila=8;
    let columna=8;
    let x;
    let y;
    let casillas;

   
    let formulario= document.getElementById("TableroBuscaminas");
    for(x=0;x<columna;x++)
    {
        for(y=0;y<fila;y++)
        {
            casillas=document.createElement("input");
            casillas.setAttribute("type","button");
            casillas.setAttribute("id","casilla"+ x +" "+ y);
            casillas.setAttribute("onclick","miTablero.ocultarCelda(this.id)");
            formulario.appendChild(casillas);
        }
    }
 }
 ocultarCelda(id) 
 {
    let celda;
    celda = document.getElementById(id);
    celda.setAttribute("style", "visibility:hidden");
 }
}

let miTablero=new Tablero();
miTablero.IniciarTablero();
miTablero.ocultarCelda(id);

/*class Sembrador
{
    campo;
    sembrarMinas()
    {
        this.campo=
        [
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0]
        ]
        let minas=10;
        while(minas>0)
        {
            let fila=Math.floor(Math.random()*8);
            let columna=Math.floor(Math.random()*8);

            if(this.campo[fila][columna]===0)
            {
                this.campo[fila][columna]=1;
                minas--;
            }
        }
    }
}
let miSembrador=new Sembrador();
miSembrador.sembrarMinas();*/

class Campo
{
    campo=Array();
    tamaño;


    constructor()
    {
        this.tamaño=9;
    }

    crearCampo()
    {
        for(let i=0;i<this.tamaño;i++)
        {
            for(let k=0;k<this.tamaño;k++)
            {
                if(!this.campo[i])
                {
                    this.campo=Array();
                }
                this.campo[i][k]=new Casilla();
            }
        }
    }
}
let miCampo= new Campo();
miCampo.crearCampo();

class Sembrador
{
    /**
     * unCampo es un objeto que Sembrador usara para sembrar
     * @type {Campo} 
     */
    unCampo;
    /**
     * Minas que deben ser sembradas en unCampo
     * @type {number}
     */
    cantidadMinas;

    constructor(elCampo="", minas=10)
    {
        if(elCampo==="")
        {
            throw new Error("No puedo seguir porque no me asigno un campo?")
        }
        this.unCampo=elCampo;
        if(minas>0 && minas<Math.pow(this.unCampo.tamaño,2))
        {
            this.cantidadMinas=minas;
        }
        else
        {
            this.cantidadMinas=Math.round(Math.pow(this.campo.tamaño,2)*0.1);
        }
    }

    sembraMinas()
    {
        while(this.cantidadMinas>0)
        {
            let fila=Math.floor(Math.random()*this.unCampo.tamaño)
            let columna=Math.floor(Math.random()*this.unCampo.tamaño)

            if(this.unCampo[fila][columna].minada===false)
            {
                this.unCampo[fila][columna].minada===true;
                this.cantidadMinas--;
            }
        }
    }
}
let miSembrador= new Sembrador(miCampo);
miSembrador.sembraMinas();
class Casilla
{
    estado;
    bombasAlrededor;
    minada;

    constructor()
    {
        this.estado="cubierta";
        this.bombasAlrededor=0;
        this.minada=false;
    }

}
let miCasilla= new Casilla();

